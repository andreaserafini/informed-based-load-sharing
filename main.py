from EdgeNode import EdgeNode, bcolors
import time
import sys

def main():

    if len(sys.argv) != 2:
        print("WRONG ARGUMENT NUMBER: <python3 main.py <num_servers>>")
        return 
    
    num_servers = int(sys.argv[1])
    counter = 0
    servers = []
    for i in range(2000, 2000 + num_servers * 3, 3):
        server = EdgeNode(
            int_hp=('127.0.0.1', i),
            frw_hp=('127.0.0.1', i+1),
            ext_hp=('127.0.0.1', i+2),
            known_servers_list=[str(str(el.ip) + ":" + str(el.port)) for el in servers],
            color=counter
        )

        server.activate()
        servers.append(server)
        counter += 1

if __name__ == "__main__":
    main()