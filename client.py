import socket
import sys
import threading
import time

HOST, PORT = "localhost", int(sys.argv[1])
data = "".join(sys.argv[2:])

def send_request():
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
        # Connect to server and send data
        sock.connect((HOST, PORT))
        start = time.time()
        sock.sendall(bytes(data, encoding="utf-8"))

        # Receive data from the server and shut down
        received = str(sock.recv(1024), "utf-8")
        end = time.time()

    if data.upper() != received:
        print("\033[91m" + f"ERROR\tin {end - start} {received} != {data}.upper()" + "\033[0m")
    else:
        print("\033[92m" + f"OK\tin {end - start}" + "\033[0m")



if __name__ == "__main__":
    t = []
    for i in range(50):
        t.append(threading.Thread(target=send_request))
        t[-1].start()

    for i in range(50):
        t[i].join()
