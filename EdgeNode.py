import datetime
import select
import socket
import threading
from queue import PriorityQueue
import random
import time

from ClientRequestHandler import ClientRequestHandler

class bcolors:
    colors = \
    [
        '\033[95m',
        '\033[94m',
        '\033[96m',
        '\033[92m',
        '\033[93m',
        '\033[0m',
    ]
    
class ServerInfo:
    
    def __init__(self, int_ip, int_port, frw_ip, 
        frw_port, load, capacity, timestamp) -> None:
        self.int_ip
        


class EdgeNode:

    _NUM_WORKERS = 4
    _UPDATER_PERIOD = 0.5
    _SERVER_UPDATE_TTL = 1
    _REQS_QUEUE_CAP = 10

    def __init__(self, 
        int_hp=('localhost', 2345),
        ext_hp=('localhost', 2347),
        frw_hp=('localhost', 2346),
        known_servers_list=[],
        color=0) -> None:

        self.ip = str(int_hp[0])
        self.port = str(int_hp[1])

        if(color < len(bcolors.colors)):
            self.color = bcolors.colors[color]
        else:
            self.color = bcolors.colors[0]

        self.srv_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.cli_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.frw_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        self.srv_socket.bind(int_hp)
        self.cli_socket.bind(ext_hp)
        self.frw_socket.bind(frw_hp)

        self.srv_socket.listen(50)
        self.cli_socket.listen(50)
        self.frw_socket.listen(50)

        self.sockets = [self.srv_socket, self.cli_socket, self.frw_socket]

        self.reqs_queue = PriorityQueue(maxsize=EdgeNode._REQS_QUEUE_CAP)

        self.workers = list()
        for _ in range(EdgeNode._NUM_WORKERS):
            th = ClientRequestHandler(self.reqs_queue, self.color)
            th.start()
            self.workers.append(th)
        
        # Add known servers to the dictionary 
        self.servers = dict()
        for el in known_servers_list:
            ip = el.split(":")[0]
            port = int(el.split(":")[1])
            self.servers[(ip, port)] = ("", 0, 0, -1, 0)

        self.updater = threading.Thread(target=self.send_updates)
        self.listener = threading.Thread(target=self.serve)

        self.lock = threading.Lock()

        self.rr_counter = 0


    def send_updates(self):

        frw_ip = str(self.frw_socket.getsockname()[0])
        frw_port = str(self.frw_socket.getsockname()[1])
        int_ip = str(self.srv_socket.getsockname()[0])
        int_port = str(self.srv_socket.getsockname()[1])

        # update msg format: "INT_IP:INT_PORT:FRW_IP:FRW_PORT:LOAD:CAPACITY"
        while self.updater_started:
            msg = int_ip + ":" + int_port + ":" + frw_ip + ":" + frw_port + ":" + \
                str(self.reqs_queue.qsize()) + ":" + str(self.reqs_queue.maxsize)

            self.lock.acquire()
            for el in self.servers.keys():
                sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

                srv_ip = el[0]
                srv_port = el[1]

                sock.connect((srv_ip, srv_port))
                sock.sendall(bytes(msg, "utf-8"))
                sock.close()
            self.lock.release()
            
            time.sleep(EdgeNode._UPDATER_PERIOD)


    def parse_update(self, conn):

        data = str(conn.recv(1024), "utf-8")
        # self.echo(f"data: {data}")

        int_ip = data.split(":")[0]
        int_port = int(data.split(":")[1])
        frw_ip = data.split(":")[2]
        frw_port = int(data.split(":")[3])
        load = int(data.split(":")[4])
        capacity = int(data.split(":")[5])
        
        timestamp = time.time()
        self.lock.acquire()
        self.servers[(int_ip, int_port)] = (frw_ip, frw_port, load, capacity, timestamp)
        self.lock.release()

        # self.echo(self.servers)


    def echo(self, str):
        print(f"{self.color}[SERVER {self.ip}, {self.port}]: {str}{bcolors.colors[-1]}")


    def get_ordered_srv_list(self):
        self.lock.acquire()
        # order server by load level
        sorted_server_list = \
            [self.servers[x] for x in sorted(self.servers.keys(),
            key=lambda item: self.servers[item][2] / self.servers[item][3])]
        # self.echo(sorted_server_list)
        self.lock.release()
        
        # avoid dead servers
        alive_servers = []
        for el in sorted_server_list:
            if time.time() - el[4] <= 0.5:
                alive_servers.append(el)
        
        return alive_servers


    # TODO: implement round robin
    def least_loaded_frw(self, conn=None):
        if conn != None:
            data = str(conn.recv(1024), "utf-8")
        else:
            self.echo("connection error, None")
            return

        backoff = 0.1
        received = ""
        while (received == "__NACK" or received == ""):
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

            serv_list = self.get_ordered_srv_list()
            if len(serv_list) == 0:
                self.echo("no available server, sending __ERR")
                conn.sendall(bytes("__ERR", encoding="utf_8"))
                return

            serv = serv_list[0]

            # self.echo(type(serv))
            sock.connect((serv[0], serv[1]))
            sock.sendall(bytes(data, encoding="utf-8"))
            

            # Receive data from the server and shut down
            received = str(sock.recv(1024), "utf-8")
            # self.echo(f"received: {received}")

            sock.close()

            if received == "__NACK" or received == "":
                time.sleep(backoff)
                backoff *= 2

        if conn != None:
            conn.sendall(bytes(received, encoding="utf_8"))
    
    # avoid herdng effect
    def k_least_loaded_frw(self, conn=None, k=2):

        if conn != None:
            data = str(conn.recv(1024), "utf-8")
        else:
            self.echo("connection error, None")
            return

        backoff = 0.1
        received = ""
        while (received == "__NACK" or received == ""):
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            
            serv_list = self.get_ordered_srv_list()
            # self.echo(serv_list)

            if len(serv_list) == 0:
                self.echo("no available server, sending __ERR")
                conn.sendall(bytes("__ERR", encoding="utf_8"))
                return

            if len(serv_list) < k:
                self.echo("reducing k parameter to server list length")
                k = len(serv_list)
            serv = serv_list[random.randint(0, k - 1)]

            # self.echo(serv_list)
            # self.echo(serv)

            sock.connect((serv[0], serv[1]))
            sock.sendall(bytes(data, encoding="utf-8"))
            

            # Receive data from the server and shut down
            received = str(sock.recv(1024), "utf-8")
            # self.echo(f"received: {received}")

            sock.close()

            if received == "__NACK" or received == "":
                time.sleep(backoff)
                backoff *= 2

        if conn != None:
            conn.sendall(bytes(received, encoding="utf_8"))

    def round_robin_frw(self):
        pass

    # TODO: establish socket priority here -> server update > requests
    def serve(self):
        self.echo(f"serving on [{self.cli_socket.getsockname()}]")
        while self.listener_started:
            # SERVER blocks here waiting for an incoming connection 
            ready_socks,_,_ = select.select(self.sockets, [], [], 5.)
            for sock in ready_socks:
                conn, (rhost, rport) = sock.accept()
                if sock is self.cli_socket:
                    if not self.reqs_queue.full():
                        # lower priority -> (1, current time)
                        self.reqs_queue.put((1, time.time(), conn))
                        self.echo(f"enqueuing request by client {rhost}, {rport}")
                    else:
                        # queue is full -> forward
                        self.echo(f"forwarding request sent by client {rhost}, {rport}")
                        threading.Thread(target=self.k_least_loaded_frw,
                            kwargs={"conn" : conn, "k": 3}).start()

                elif sock is self.frw_socket:
                    if not self.reqs_queue.full():
                        # higher priority -> (0, current time)
                        self.reqs_queue.put((0, time.time(), conn))
                        self.echo(f"enqueuing request by server {rhost}, {rport} {self.reqs_queue.qsize()}")
                    else:
                        # queue is full -> reply "also my queue is full"
                        _ = str(conn.recv(1024), "utf-8")
                        conn.sendall(bytes("__NACK", encoding="utf-8"))
                        self.echo(f"got request by server {rhost}, {rport}, sending __NACK")
                else:
                    # SERVER MSG: host-port-connections-capacity
                    self.parse_update(conn)
                    # self.echo(f"got update by server {len(self.servers.keys())}")
                    

    def activate(self):
        self.updater_started = True
        self.listener_started = True
        self.listener.start()
        self.updater.start()

    def deactivate(self):
        self.updater_started = False
        self.listener_started = False
        self.updater.join()
        self.listener.join()


    def shutdown(self):
        self.srv_socket.close()
        self.cli_socket.close()

        for th in self.workers:
            th.cancel()
        


# TODO: protection against client connecting to a backend port
# TODO: substitute .recv with proper implementations (while ...)
