import threading
import time

class ClientRequestHandler(threading.Thread):
    
    def __init__(self, queue, color) -> None:
        super().__init__()
        self.queue = queue
        self.color = color

    def run(self) -> None:
        while True:
            conn = self.queue.get()
            self.handle_request(conn[2])
    
    def handle_request(self, conn):
        data = str(conn.recv(1024), "utf-8")
        # print("[CLIENT_HANDLER {}]: received: {}"
            # .format(threading.get_ident(), data))
        
        conn.sendall(bytes(data.upper(), encoding="utf-8"))

        print(self.color + f"[THREAD {threading.get_ident()}]: serving request" + '\033[0m')

        # print("[CLIENT_HANDLER {}]: data sent, sleeping now"
        #     .format(threading.get_ident()))

        time.sleep(1)
        
        # print("[CLIENT_HANDLER {}]: finished sleeping, bye"
        #     .format(threading.get_ident()))

